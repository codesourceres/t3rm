import React, { Component } from 'react';
import firebase from "firebase/app";
import 'firebase/database'
import 'firebase/storage'
import Form from './components/form'
import Reports from './components/reports'
import './App.css'
import { css } from 'glamor'

import UserContext from './context/app-context'

export default class App extends Component {

    state = {
        admin: '',
        reports: [],
        reportsReady: false,
        auth: false,
        authMessage: '',
        storageRef: '',
        imagesRef: '',
        logo: '',
        loading: false,
        adminReady: false,
        connected: false,
        showConnection: false,
        users: []
    }

    componentWillMount() {
        document.title = 'T3RM'

        this.getUsers()
        this.checkConnection()
    }

    getTable = (table) => {
        const rootRef = firebase
            .database()
            .ref()
            .child(table)

        return rootRef
    }

    getUsers = () => {
        const userRef = this.getTable('users')

        userRef.on('value', snap => {
            this.setState(() => ({
                users: snap.val(),
            }))

            this.getAdmin()
            this.getReports()
        })
    }

    getAdmin = () => {
        const adminRef = this.getTable('admin')

        adminRef.on('value', snap => {
            const admin = snap.val()
            this.setState(() => ({
                admin: {
                    __name: admin.name,
                    __email: admin.email,
                    __password: admin.password
                },
            }))

            setTimeout(() => {
                this.setState(prevState => ({
                    adminReady: !prevState.adminReady
                }))

                const userCred = sessionStorage.getItem('user')
                if (userCred) {
                    this.handleAuth(JSON.parse(userCred))
                }
            }, 1000);
        })
    }

    checkConnection = () => {
        const connectedRef = firebase.database().ref(".info/connected");
        connectedRef.on("value", (snap) => {
            if (snap.val() === true) {
                // if connected
                this.console('T3rm is now connected... ☑️', 'green')

                this.setState(() => ({
                    connected: true,
                }))

                setTimeout(() => {
                    this.setState(() => ({
                        showConnection: false
                    }))
                }, 2000);
            } else {
                this.setState(() => ({
                    connected: false,
                    auth: false,
                    showConnection: true,
                }))

            }
        });
    }

    console = (message, color) => {
        console.log('%c ' + message + '', 'color: ' + color + '; padding:5px 10px; border: 1px solid ' + color + '; border-radius: 3px');
    }

    getReports = () => {

        const reportsRef = this.getTable('reports')

        reportsRef.on('value', snap => {
            this.setState({
                reports: snap.val(),
                reportsReady: true
            })

            this.sortReports()
        })

    }

    sortReports = () => {
        const { reports } = this.state
        reports.sort((a, b) => {
            let b_rid = b.report_id.toString()
            b_rid = parseFloat(b_rid.split('-')[1])
            const a_rid = parseFloat(a.report_id.split('-')[1])
            return b_rid - a_rid
        })
        this.setState({
            reports
        })
    }

    handleAuth = (cred) => {
        const { email, password } = cred

        const { __email, __password } = this.state.admin

        if (email === __email && password === __password) {
            if (this.state.connected) {
                this.setState(() => ({
                    authMessage: 'Signing in...'
                }))
                this.console('Successful! You are now signing in... 🏃', 'orange')

                const user = { email, password }
                sessionStorage.setItem('user', JSON.stringify(user))
            }

            this.handleLoading()
        } else if (email === __email && password !== __password) {
            this.setState((prevState) => ({
                auth: prevState.auth,
                authMessage: 'Sorry your password is incorrect. 🔑'
            }))

            this.console('Sorry your password 🔑 is incorrect. ', 'red')
        } else if (email !== __email && password === __password) {
            this.setState((prevState) => ({
                auth: prevState.auth,
                authMessage: 'Sorry your email is incorrect. ✉️'
            }))
            this.console('Sorry your email ✉️ is incorrect.', 'red')
        } else if (email !== __email && password !== __password) {
            this.setState((prevState) => ({
                auth: prevState.auth,
                authMessage: 'Sorry your email and password is incorrect.'
            }))
        }
    }

    handleLogout = () => {
        sessionStorage.removeItem('user')
        this.setState(() => ({
            auth: false,
            authMessage: ''
        }))
    }

    handleLoading = () => {
        this.setState((prevState) => ({
            loading: !prevState.loading
        }))

        setTimeout(() => {
            this.console('Signed In! ☑️', 'skyblue')
            this.setState((prevState) => ({
                auth: !prevState.auth,
                loading: !prevState.loading
            }))
        }, 2000);
    }

    render() {
        const values = {
            users: this.state.users,
            reports: this.state.reports
        }

        return (
            <section className={mainWrapper}>
                <UserContext.Provider value={values}>
                    {
                        this.state.auth ? '' :
                            <Form
                                onAuth={this.handleAuth}
                                adminReady={this.state.adminReady}
                                authMessage={this.state.authMessage}
                                connected={this.state.connected}
                                showConnection={this.state.showConnection}
                                loading={this.state.loading} />
                    }

                    {
                        this.state.reportsReady ?
                            this.state.auth ?
                                <Reports
                                    users={this.state.users}
                                    reports={this.state.reports}
                                    admin={this.state.admin}
                                    storage={this.state.storage}
                                    handleLogout={this.handleLogout} /> : '' : ''
                    }
                    <img className={loginBG} src="/static/images/logo.png" alt="" />
                </UserContext.Provider>
            </section>
        );
    }
}

let mainWrapper = css({
    overflowX: 'hidden'
})

let loginBG = css({
    position: 'fixed',
    bottom: 0,
    right: 0,
    zIndex: 1000,
    transform: 'translate(10%, 10%)',
    opacity: .2
})