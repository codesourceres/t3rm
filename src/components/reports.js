import React, { Component } from 'react';
import Report from './report'
import { css } from 'glamor'
import './reports.css'
import UserContext from '../context/app-context';

export default class Reports extends Component {

    state = {
        loading: false,
        user: ''
    }

    componentWillMount() {
        document.title = 'Reports | T3RM'
        document.body.style.background = 'whitesmoke'
    }

    componentWillUnmount() {
        document.title = 'T3RM'
        document.body.style.background = 'linear-gradient(#e63950, #e63950)'
    }

    handleLogout = () => {
        this.setState(prevState => ({
            loading: !prevState.loading
        }))
        setTimeout(() => {
            this.props.handleLogout()
        }, 2000);
    }

    handleUser = (user_id) => {
        const users = this.context.users
        const filtered_users = users.filter(user => {
            const _user_id_ = parseFloat(user.id) || parseFloat(user.user_id)
            return _user_id_ === user_id
        })

        return filtered_users
    }

    render() {
        const reports = this.context.reports

        const rep = reports.map((report, i) => {
            const user = this.handleUser(parseFloat(report.user_id))
            return <Report key={i} user={user} report={report} storage={this.props.storage} />
        })

        return (
            <section className={wrapper}>
                <nav className={nav}>
                    <div>
                        <div className={logoWrapper}>
                            <img src="/static/images/logo.png" alt="" />

                        </div>
                        <label className={logoTitle}>T3RM</label>
                    </div>


                    <div className={navContentWrapper}>
                        <label>Welcome, {this.props.admin.name}</label>
                        <label>|</label>
                        <button onClick={this.handleLogout}>Logout</button>
                    </div>
                    <div className={[loader, this.state.loading ? loading : ''].join(' ')}></div>
                </nav>

                <article className={reportsWrapper}>
                    <div className={reportsTitle}>
                        <h2>Reports</h2>
                        <label>Total: {this.props.reports.length}</label>
                    </div>
                    <div className={reportHeaders}>
                        <small>subject</small>
                        <small>message</small>
                        <small>date created</small>
                    </div>
                    {rep}
                </article>
            </section>
        )
    }
}

Reports.contextType = UserContext


let wrapper = css({
    position: 'relative',
    zIndex: 1001
})

let reportHeaders = css({
    display: 'grid',
    gridTemplateColumns: '35% 35% 30%',
    '> small': {
        textTransform: 'uppercase',
        display: 'grid',
        justifyContent: 'center',
        color: '#e63950'
    },
})

let reportsTitle = css({
    display: 'grid',
    gridTemplateColumns: 'auto auto',
    padding: '20px 0',
    '> h2': {
        margin: 0,
        verticalAlign: 'middle'
    },
    '> label': {
        verticalAlign: 'middle',
        display: 'grid',
        justifyContent: 'right',
        alignContent: 'center'
    }
})

let logoTitle = css({
    display: 'inline-block',
    verticalAlign: 'middle',
    marginLeft: 10,
    opacity: .3,
    fontWeight: 600
})

let logoWrapper = css({
    width: 35,
    height: 35,
    display: 'inline-block',
    verticalAlign: 'middle',
    '> img': {
        width: '100%'
    }
})

let reportsWrapper = css({
    width: '100%',
    maxWidth: 1140,
    margin: '0 auto',
    padding: 10,
    boxSizing: 'border-box',
    background: 'whitesmoke',
})

let loading = css({
    width: '100% !important'
})

let loader = css({
    width: '0%',
    position: 'absolute',
    bottom: 0,
    left: 0,
    height: 5,
    background: '#e63950',
    transition: '2s ease'
})

let navContentWrapper = css({
    textAlign: 'right',
    '> label': {
        marginRight: 10
    },
    '> button': {
        borderStyle: 'none',
        padding: 10,
        background: 'rgba(0,0,0,0)',
        color: '#777',
        cursor: 'pointer',
        transition: '300ms ease',
        ':hover': {
            background: '#e63950',
            color: 'white'
        }
    }
})

let nav = css({
    width: '100%',
    background: '#222',
    padding: 20,
    boxSizing: 'border-box',
    color: 'white',
    position: 'relative',
    display: 'grid',
    gridTemplateColumns: 'auto auto'
})

