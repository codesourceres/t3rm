import React, { Component } from 'react';
import ReportModal from './reportModal'
import { css } from 'glamor'
import moment from 'moment'
import firebase from "firebase/app";
import 'firebase/database'
import 'firebase/storage'

import UserContext from '../context/app-context'

export default class Report extends Component {

    state = {
        showModal: false,
        active: false,
        dots: false,
        attachment: '',
        ready: true,
        noAttachment: false
    }

    componentDidMount() {
        // console.log(this.props.user);
    }

    componentWillMount() {
        this.handleDots()
        this.handleAttachment()
    }

    handleAttachment = () => {
        const { attachment_type, attachment_url } = this.props.report

        if (attachment_type === 1 && attachment_url !== '') {
            this.attachmentRef('attachments/images', attachment_url)
        } else if (attachment_type === 2 && attachment_url !== '') {
            this.attachmentRef('attachments/videos', attachment_url)
        } else {
            this.setState(() => ({
                noAttachment: true
            }))
        }
    }

    attachmentRef = (type, filename) => {
        const { attachment_type } = this.props.report
        const storageRef = firebase.storage().ref()
        const attachmentRef = storageRef.child(type)
        const attachmentResult = attachmentRef.child(filename)

        attachmentResult.getDownloadURL().then(url => {
            if (url) {
                if (attachment_type === 1) {
                    const img = new Image()
                    img.src = url
                    img.onload = () => {
                        this.setState(() => ({
                            attachment: img,
                            ready: true
                        }))
                    }
                } else {
                    let video = document.createElement("VIDEO")

                    video.setAttribute("src", url);
                    video.setAttribute("width", "100%");
                    video.setAttribute("height", "366px");
                    video.setAttribute("controls", "controls");

                    this.setState(() => ({
                        attachment: video,
                        ready: true
                    }))
                }
            }
        })
            .catch(err => { })
    }

    handleTitleClick = () => {
        this.setState(prevState => ({
            showModal: !prevState.showModal,
            active: !prevState.active
        }))
    }

    handleDots = () => {
        let dots = { status: false }
        const limit = 60
        const msgSplit = this.props.report.message.split('')

        if (msgSplit.length >= limit) {
            dots.status = true
        }

        this.setState({
            dots: dots.status,
        })
    }

    limitMsg = (msg) => {
        const messageSplited = msg.split('')
        const limit = 60

        let newMsg = []
        messageSplited.map(m => {
            if (newMsg.length < limit) {
                newMsg.push(m)

            }

            return true
        })

        return newMsg.join('')
    }

    render() {
        const { subject, message, date_created } = this.props.report
        const msg = this.limitMsg(message)
        return (
            <section className={reportWrapper}>
                {
                    this.state.ready ?
                        <div className={[reportTitle, this.state.active ? active : ''].join(' ')} onClick={this.handleTitleClick}>
                            <h2 >{subject} </h2>
                            <p>{msg}{this.state.dots ? '...' : ''}</p>
                            <small>{moment(date_created).format('MMMM DD YYYY, h:mm:ss A')}</small>
                        </div> : <img src="/static/images/loader-2.gif" className={loaderGIF} alt="" />
                }

                {
                    this.state.showModal ?
                        <ReportModal
                            noAttachment={this.state.noAttachment}
                            attachment={this.state.attachment}
                            onModalClose={this.handleTitleClick}
                            user={this.props.user}
                            report={this.props.report} /> : ''
                }
            </section>
        )
    }
}

Report.contextType = UserContext

let loaderGIF = css({
    width: 60,
    margin: '0 auto',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
})

let reportWrapper = css({
    marginBottom: 10,
    position: 'relative',
    minHeight: 42,
    background: '#fff',
    margin: '10px 0',
    borderRadius: 5,
    border: '1px solid rgba(0,0,0,0)',
    boxShadow: '0 1px 5px rgba(0,0,0,.1)',
    transition: '250ms ease',
    ':hover': {
        color: '#e63950',
        border: '1px solid #e63950',
        boxShadow: '0 5px 10px rgba(0,0,0,.2)',
    },
})

let active = css({
    background: '#e63950 !important',
    color: 'white',
    border: '1px solid #e63950'
})

let reportTitle = css({
    cursor: 'pointer',
    position: 'relative',
    display: 'grid',
    gridTemplateColumns: '35% 35% 30%',
    padding: '10px',

    '> h2': {
        display: 'grid',
        margin: 0,
        fontSize: '1em',
        alignContent: 'center'
    },
    '> small': {
        display: 'grid',
        alignContent: 'center',
        justifyContent: 'center'
    },
    '> p': {
        margin: 0,
        display: 'grid',
        alignContent: 'center',
        justifyContent: 'left',
        fontSize: 13
    }
})