import React, { Component } from 'react'
import { css } from 'glamor'

export default class Form extends Component {
    state = {
        showConnection: false
    }

    handleSubmit = (e) => {
        e.preventDefault()
        const cred = { email: this.email.value, password: this.password.value }
        this.props.onAuth(cred)
    }

    render() {

        return (
            <section className={signInWrapper}>
                <label
                    className={connection}
                    style={{
                        background: `${this.props.connected ? '#5bc980' : '#ff637b'}`,
                        height: `${this.props.showConnection ? '30px' : '0px'}`
                    }}
                >
                    {this.props.connected ? "Connected" : "Not Connected"}
                </label>

                <figure className={figure}>
                    {/* <img src={this.props.logoURL} alt="" /> */}
                    <img src="/static/images/logo.png" alt="" />
                </figure>

                <form onSubmit={this.handleSubmit} className={formWrapper}>
                    <label className={signinTitle}>SIGN IN | T3RM</label>

                    {
                        this.props.authMessage.length > 0 ?
                            <label
                                style={this.props.authMessage === 'Signing in...' ?
                                    { background: '#5bc980' } : { background: '#ff637b' }}>
                                {this.props.authMessage}</label> : ''
                    }

                    <div className={inputWrapper}>
                        <input ref={email => (this.email = email)} type="text" placeholder="email@company.com" />
                    </div>
                    <div className={inputWrapper}>
                        <input ref={password => (this.password = password)} type="password" placeholder="********" />
                    </div>
                    <div className={inputWrapper}>
                        {
                            this.props.adminReady ? null :
                                <button disabled style={{ background: 'white', cursor: 'progress', height: 36 }}>
                                    <img src="/static/images/loader-2.gif" className={loaderGIF} alt="" />
                                </button>
                        }
                        {
                            this.props.adminReady ? this.props.loading ?
                                <button disabled style={{ background: 'white', cursor: 'progress', height: 36 }}>
                                    <img src="/static/images/loader-2.gif" className={loaderGIF} alt="" />
                                </button> :
                                <button style={{ height: 36 }}>Sign in</button> : ''
                        }
                    </div>
                </form>
                <div className={[loader, this.props.loading ? loading : ''].join(' ')}></div>
            </section>
        )
    }
}

let connection = css({
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    display: 'grid',
    textAlign: 'center',
    transform: 'translateY(-100%)',
    height: 30,
    fontWeight: 600,
    color: 'white',
    background: '#ff637b',
    boxSizing: 'border-box',
    zIndex: 1000,
    alignContent: 'center',
    overflow: 'hidden',
    transition: '500ms',
})

let loaderGIF = css({
    width: 60,
    margin: '0 auto',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
})

let signinTitle = css({
    display: 'block',
    margin: 0,
    textAlign: 'center',
    color: '#e63950 !important',
    fontWeight: 600
})

let loading = css({
    width: '100% !important'
})

let loader = css({
    width: '0%',
    position: 'absolute',
    bottom: 0,
    left: 0,
    height: 5,
    background: '#5bc980',
    transition: '2s ease'
})

let inputWrapper = css({
    marginBottom: 10,
    '> input': {
        borderStyle: 'none',
        width: '100%',
        boxSizing: 'border-box',
        padding: 10,
        border: '1px solid #f9f9f9',
        transition: '300ms',
        ':focus': {
            outline: 'none',
            boxShadow: 'none',
            borderColor: '#eebf00'
        }
    },
    '>button': {
        borderStyle: 'none',
        padding: 10,
        width: '100%',
        background: '#5bc980',
        color: 'white',
        cursor: 'pointer',
        transition: '300ms',
        position: 'relative',
        ':hover': {
            background: '#51b071'
        }
    }
})

let formWrapper = css({
    width: '100%',
    boxSizing: 'border-box',
    marginTop: 30,
    '> label': {
        textAlign: 'center',
        display: 'block',
        marginBottom: 10,
        padding: 5,
        color: 'white'
    }
})

let figure = css({
    margin: '0 auto',
    height: 140,
    width: 140,
    '> img': {
        width: '100%'
    }
})

let signInWrapper = css({
    background: 'white',
    width: '80%',
    maxWidth: 400,
    boxShadow: '0 15px 45px rgba(0,0,0,.1)',
    padding: 30,
    boxSizing: 'border-box',
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    zIndex: 1001
})