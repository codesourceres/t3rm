import React, { Component } from 'react';
import { css } from 'glamor'
import moment from 'moment'

export default class ReportModal extends Component {

    state = {
        user: ''
    }

    handleCloseModal = () => {
        this.props.onModalClose()
    }

    componentDidMount() {
        if (this.props.noAttachment) {
            const txt = document.createElement('h1')
            txt.innerHTML = 'No Attachment Available'
            txt.setAttribute("class", noAttachment)
            document.querySelector('.attachment').appendChild(txt)
        }

        if (this.props.attachment) {
            document.querySelector('.attachment').appendChild(this.props.attachment)
        }

    }

    componentWillMount() {
        this.setState({
            user: this.props.user[0]
        })
    }

    componentDidUpdate() {
        if (this.props.noAttachment) {
            const txt = document.createElement('h1')
            txt.innerHTML = 'No Attachment Available'
            txt.setAttribute("class", noAttachment)
            document.querySelector('.attachment').appendChild(txt)
        } else {
            document.querySelector('.attachment').appendChild(this.props.attachment)
        }

        if (this.props.attachment) {
            document.querySelector('.attachment').appendChild(this.props.attachment)
        }

    }

    render() {
        const report = this.props.report
        const user = this.state.user
        const usr = user ? user : { fname: 'N/A', lname: 'N/A', usertype: 'N/A' }
        const { subject, message, date_created, report_id } = report
        const violation = report.violation ? report.violation : <small style={{ color: '#ccc' }}>N/A</small>
        const { fname, lname, usertype } = usr

        return (
            <section className={reportModalWrapper}>
                <button className={closeBtn} onClick={this.handleCloseModal}>Close</button>

                <article className={reportModalContent}>

                    <div
                        style={this.props.noAttachment ? { padding: 40 } : this.props.attachment ? { paddingBottom: 0 } : { paddingBottom: '50%' }}
                        className={[attachmentWrapper, 'attachment'].join(' ')}>
                    </div>

                    <div className={detailsWrapper}>
                        <small className={detailsTitle}>DATE:</small>
                        <h4>{moment(date_created).format('MMMM DD YYYY, h:m:ss A')}</h4>
                    </div>

                    <div className={detailsWrapper}>
                        <small className={detailsTitle}>REPORTER DETAILS:</small>
                        <h4><small>Name:</small> {fname} {lname}</h4>
                        <h4><small>Type:</small> {usertype}</h4>
                    </div>

                    <div className={detailsWrapper}>
                        <small className={detailsTitle}>VIOLATION:</small>
                        <h4>{violation}</h4>
                    </div>

                    <div className={detailsWrapper}>
                        <small className={detailsTitle}>SUBJECT:</small>
                        <h4>{subject}</h4>
                    </div>

                    <div className={detailsWrapper}>
                        <small className={detailsTitle}>MESSAGE:</small>
                        <h4>{message}</h4>
                    </div>

                    <small>REPORT ID: {report_id}</small>

                </article>
            </section>
        )
    }
}

let noAttachment = css({
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    textTransform: 'uppercase',
    fontSize: '1.5em',
    color: '#ccc',
    width: '100%',
    textAlign: 'center',
    margin: 0,
    boxSizing: 'border-box'
})

const modalTxtColor = '#bbb'

let detailsTitle = css({
    display: 'block',
    marginBottom: 5,
    position: 'absolute',
    top: 0,
    left: 10,
    background: 'white',
    padding: '0 10px',
    transform: 'translateY(-50%)',
    zIndex: 1002,
    fontSize: '.7em',
    color: modalTxtColor
})

let attachmentWrapper = css({
    width: '100%',
    background: '#eee',
    marginBottom: 20,
    position: 'relative',
    boxSizing: 'border-box',
    '> img': {
        width: '100%',
        display: 'block'
    }
})

let closeBtn = css({
    position: 'absolute',
    bottom: 20,
    left: '50%',
    boxShadow: '0 10px 20px rgba(0,0,0,.1)',
    borderStyle: 'none',
    background: 'white',
    transition: '300ms ease',
    transform: 'translateX(-50%)',
    fontWeight: 600,
    borderRadius: 5,
    padding: '10px 20px',
    cursor: 'pointer',
    zIndex: 1000,
    color: '#e63950',
    ':hover': {
        background: '#e63950',
        color: 'white',
        boxShadow: '0 20px 35px rgba(0,0,0,.4)',
    }
})

let detailsWrapper = css({
    position: 'relative',
    margin: '10px 0 30px',
    border: `1px solid ${modalTxtColor}`,
    borderRadius: 5,
    padding: '1em',
    boxSizing: 'border-box',
    '> h4': {
        margin: 0,
        padding: 10,
        maxHeight: 250,
        overflowY: 'auto',
        fontWeight: 'normal',
        '> small': {
            minWidth: 40,
            display: 'inline-block'
        }
    }
})

let reportModalWrapper = css({
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    zIndex: 2,
    background: 'rgba(0,0,0,.8)',
    color: 'black'
})

let reportModalContent = css({
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    background: 'white',
    boxShadow: '0 15px 45px rgba(0,0,0,.5)',
    padding: 30,
    width: '80%',
    maxWidth: 750,
    maxHeight: '70vh',
    overflowY: 'auto',
    zIndex: 1001,
    borderRadius: 5,
    '> img': {
        width: '100%'
    },
    '> small': {
        display: 'block',
        textAlign: 'right'
    }
})